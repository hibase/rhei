require 'thread'

module Rhei

  class Connection

    def initialize(back_pressure_threshold: 10_000, size_threshold: "1gb")
      @mode = mode
      @queue = Queue.new
      @back_pressure_threshold = back_pressure_threshold
    end

    def <<(ip)
      sleep 1 while @queue.size >= @back_pressure_threshold
      @queue << ip
    end

    def next
      @queue.pop(true)
    rescue ThreadError
      nil
    end

  end

end

