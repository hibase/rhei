module Rhei
  class KeyExpr
    def initialize(expr, *flags)
      @expr = expr
      @flags = flags.flatten
    end

    def apply(hash)
      result = reduce(hash)
      return result if @expr.end_with?(".*")
      case result.size
      when 0 then nil
      when 1 then result.values.first.dup
      else
        result
      end
    end

    def reduce(hash)
      result = if @expr == ".*"
        hash
      else
        hash.select { |key, _| self === key.to_s }
      end
      result = dup(result)
      result = if @flags.include?(:flatten)
        flatten(result)
      else
        result
      end
      result = if @flags.include?(:strip)
        result.map { |key, value| [strip(key), value] }.to_h
      else
        result
      end
      result
    end

    def ===(other)
      comparator === other
    end

    def comparator
      @comparator ||= if @expr.end_with?(".*")
        Regexp.new("^#{@expr.sub(/\.\*$/, '')}(\..+)*$")
      elsif @expr.end_with?(".+")
        Regexp.new("^#{@expr.sub(/\.\+$/, '')}(\..+)+$")
      else
        @expr
      end
    end

    private

    def dup(selection)
      selection.inject({}) do |h, (k, v)|
        h[k.to_s] = v.dup
        h
      end
    end

    def strip(key)
      @strip ||= Regexp.new("^#{@expr.sub(/[*+]$/, '')}")
      key.to_s.sub(@strip, '')
    end

    def flatten(hash)
      result = {}
      stack = hash.to_a
      while (key, value = stack.shift) do
        if value.is_a?(Hash)
          value.each { |k, v| stack.push(["#{key}.#{k}", v]) }
        else
          result[key] = value
        end
      end
      result
    end

  end
end
