require 'json'

module Rhei
  module Loader
    class Invalid <  StandardError; end

    @context = Rhei::Context.new(
      required_paths: Set.new,
      declarations: Hash.new
    )

    def required_paths
      @context.get(:required_paths)
    end

    def declare_kind(kind, declaration)
      @context.with(:declarations) do |declarations|
        declarations[kind] = declaration
      end
    end

    def get_declaration(kind)
      @context.get(:declarations)[kind]
    end

    def require_file(path)
      @context.with(:required_paths) do |required_paths|
        if required_paths.member?(path)
          warn "already loaded: #{path}"
          return
        end
        result = Rhei::Loader.load_file(path)
        required_paths.add(path)
        result
      end
    end

    def load_file(path)
      case path
      when /.ya?ml$/
        raw = YAML.load_file(path)
        load_declaration(path, raw)
      when /.json$/
        raw = JSON.load(File.read(path))
        load_declaration(path, raw)
      when /.rb$/
        Kernel.load(path)
        path
      else
        raise Invalid, "cannot load file, unknown type: #{path}"
      end
    rescue Invalid
      raise
    rescue StandardError => e
      raise Invalid, "error loading file: #{path}\n  #{e.class.name}: #{e.message}\n    #{e.backtrace.join("\n    ")}"
    end

    def require_includes(directory, includes)
      paths = includes.flat_map do |glob|
        Dir.glob(File.join(directory, glob)).to_a
      end
      paths.each { |path| Rhei::Loader.require_file(path) }
    end

    def load_declaration(path, raw)
      raise Invalid, "declaration must be kind of Hash, got: #{raw.class.name}" \
        unless raw.kind_of?(Hash)
      raw["path"] = path
      spec = Rhei::Spec.new(raw, klass: Rhei::Declaration)
      spec.validate!
      Rhei::Declaration.new(*spec.parameters)
    end

    def cast_kind(kind_or_klass)
      return kind_or_klass if kind_or_klass.kind_of?(Class)
      lookup_kind(kind_or_klass)
    end

    def lookup_kind(kind)
      if declaration = get_declaration(kind)
        return declaration.klass
      end
      class_name = kind.gsub('.', '::')
      Object.const_get(class_name)
    rescue NameError
      raise Invalid, "cannot constantize #{class_name} (kind: #{kind}). Is it loaded?"
    end

    extend self
  end
end

