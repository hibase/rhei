require 'yaml'

module Rhei
  class Declaration

    attr_reader :name,
      :kind,
      :declares,
      :path,
      :klass,
      :spec,
      :includes

    def initialize(kind:, declares: nil, name: nil, spec: nil, path:, includes: nil)
      @kind = kind
      @name = name
      @path = path
      @klass = Rhei::Loader.cast_kind(kind)
      @spec = Rhei::Spec.new(spec, klass: klass)
      @directory = File.expand_path(File.dirname(path))
      initialize_includes(includes)
      initialize_declares(declares)
    end

    def instance
      @spec.instance
    end

    private

    def initialize_includes(includes)
      includes = includes.kind_of?(Array) ?  includes : [includes].compact
      return @includes = [] if includes.empty?
      @includes = Rhei::Loader.require_includes(@directory, includes)
    end

    def initialize_declares(declares)
      @declares = declares || @kind
      Rhei::Loader.declare_kind(@declares, self)
    end
  end
end

