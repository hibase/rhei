module Rhei
  module Expressions

    class RubyJQ
      def self.eval(expression, attributes: nil, services: nil, data: {})
        source = case data
        when Array, Hash
          Oj.dump(data)
        when String
          data
        else
          raise ExpressionLanguages::InvalidData, "data must be kind of Array, Hash or JSON-string"
        end
        JQ(source)
        jq.search(expression)
      end
    end

  end
end

