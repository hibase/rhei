require 'mustache'

module Rhei
  module Expressions

    class Mustache
      def self.eval(expression, attributes: {}, data: {}, services: {})
        ::Mustache.render(expression, attributes: attributes, services: services, data: data)
      end
    end

  end
end

