module Rhei
  class Service
    SCHEMA = {
      "type" => "object",
      "properties" => {
        "attributes" => {
          "type" => "array",
          "items" => {
            "type" => "object",
            "properties" => {
              "name" => { "$ref" => "#/definitions/attributeName" }
            },
            "additionalProperties" => false
          }
        }
      }
    }
  end
end

