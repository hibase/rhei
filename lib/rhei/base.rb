module Rhei
  class Base

    def self.properties
      if const_defined?("PROPERTIES")
        const_get("PROPERTIES")
      else
        props = ancestors.reverse.reduce({}) do |m, klass|
          klass.const_defined?("PROPERTIES") ? m.merge(klass.const_get("PROPERTIES")) : m
        end
        const_set("PROPERTIES", props)
      end
    end

    def self.spec(key, schema)
      properties[key] = schema
    end

  end
end

