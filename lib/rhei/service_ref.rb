module Rhei
  class ServiceRef

    attr_reader :name, :kind, :klass, :spec

    def initialize(name:, kind:, spec:)
      @name = name.freeze
      @kind = kind.freeze
      @klass = Rhei::Loader.cast_kind(kind, spec)
      @spec = Rhei::Spec.new(spec, klass: klass)
    end

    def instance
      @spec.instance
    end

  end
end

