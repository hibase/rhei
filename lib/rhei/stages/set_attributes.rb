module Rhei
  module Stages

    class SetAttributes
      def initialize(lang: nil, **args)
        @lang = lang || "Rhei.ExpressionLanguages.Noop"
        @attributes = args
      end

      def handler
        @handler ||= Rhei::ExpressionLanguages.get(@lang)
      end

      def call(ip)
        @attributes.each do |key, value|
          evaluated = handler.eval_data({
            "attributes" => ip[".*"]
          }, value)
          ip.set(key, evaluated)
        end
        ip
      end
    end

  end
end

