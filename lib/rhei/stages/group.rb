module Rhei
  module Stages
    class Group < Rhei::Base
      class StageNotFound < StandardError; end
      class Invalid < StandardError; end

      class Connection
        class Invalid < StandardError; end

        def self.cast_route(route)
          return :ok if route.nil?
          return route.to_sym if route.is_a?(String)
          return route if route.is_a?(Symbol)
          raise Invalid, "route must be either String or Symbol, got: #{route.inspect}"
        end

        def initialize(subscriber: nil, to: nil, route: nil)
          @subscriber, @to = subscriber, to
          @route = Connection.cast_route(route)
        end

        def subscriber(subscriber = nil)
          return @subscriber = subscriber if subscriber
          @subscriber
        end

        def to(to, route = :ok)
          @to, @route = to, Connection.cast_route(route)
        end

        def subscribed_to?(label, route:)
          @to == label && (@route == Connection.cast_route(route) || @route == :*)
        end

        def subscribers
          [@subscriber]
        end
      end

      attr_reader :schedule, :services

      spec :stages, "type" => "array", "items" => { "$ref" => "#/definitions/stage" }
      spec :graph, "type" => "array", "items" => { "$ref" => "#/definitions/graph" }
      spec :schedule, "type" => "object", "additionalProperties" => { "$ref" => "#/definitions/cron" }
      spec :services, "type" => "array", "items" => { "$ref" => "#/definition/service" }
      spec :engine, "$ref" => "#/definitions/kind"

      def initialize(stages: nil, graph: nil, schedule: nil, services: nil, engine: nil)
        @schedule = schedule || {}
        @engine = Rhei::Loader.lookup_kind(engine || "Rhei.Engine.SingleThreadedStack")
        initialize_stages(stages || [])
        initialize_graph(graph || [])
        initialize_services(services || [])
      end

      def add_stage(stage_spec)
        name = stage_spec["name"]
        raise Invalid, "stage with name `#{name}' already exists" \
          if @stages.has_key?(name)
        @stages[name] = Rhei::Spec.new(stage_spec, klass: Rhei::Stage).instance
        self
      end

      def add_service(name, kind, spec)
        @services << Rhei::ServiceRef.new(name: name, kind: kind, spec: spec)
        self
      end

      def connect(name, to:, route: :ok)
        ensure_stage(name)
        ensure_stage(to)
        connection = Connection.new(subscriber: name, to: to, route: route)
        @graph << connection
        self
      end

      def enumerate(name, from:, route: :ok)
        connection = Connection.new(to: from, route: route)
        add_stage("name" => connection.object_id, "kind" => 'Rhei.Stages.Enumerator', "spec" => {"id" => connection.object_id})
        connection.subscriber(connection.object_id)
        @graph << connection

        enumeration = Connection.new(subscriber: connection.object_id, to: connection.object_id, route: :ok)
        @graph << enumeration

        @graph << Connection.new(subscriber: name, to: connection.object_id, route: :ok)
        self
      end

      def buffer(name, from:, spec: nil, route: :ok)
        connection = Connection.new(to: from, route: route)
        buffer_spec = (spec || {}).merge("id" => connection.object_id)
        add_stage("name" => connection.object_id, "kind" => 'Rhei.Stages.Buffer', "spec" => buffer_spec)
        connection.subscriber(connection.object_id)
        @graph << connection

        add_stage("name" => name, "kind" => "Rhei.Stages.Passthrough")
        @graph << Connection.new(subscriber: name, to: connection.object_id, route: :flush)
        self
      end

      def get_stage(name)
        ensure_stage(name)
        @stages[name]
      end

      def next_stages_for(label, route: :ok)
        labels = @graph.inject([]) do |list, connection|
          list |= connection.subscribed_to?(label, route: route) ? connection.subscribers : []
        end
        labels.map(&method(:get_stage))
      end

      def call(ip = Rhei::IP.new)
        runner = @engine.new(group: self)
        runner.invoke(get_stage("IN"), ip)
      end

      private

      def ensure_stage(name)
        return if @stages.has_key?(name)
        raise StageNotFound, "stage with name `#{name}' does not exist"
      end

      def initialize_stages(list)
        @stages = {}
        list.each { |stage| add_stage(stage) }
      end

      def initialize_graph(graph)
        @graph = []
        graph.each do |connection|
          case connection["kind"]
          when "enumerate"
            enumerate(connection["stage"], from: connection["from"], route: connection["route"])
          else
            connect(connection["stage"], to: connection["to"], route: connection["route"])
          end
        end
      end

      def initialize_services(services)
        @services = []
        services.each do |service|
          add_service(service["name"], service["kind"], service["spec"])
        end
      end

    end
  end
end

