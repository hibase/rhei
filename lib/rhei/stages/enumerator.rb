module Rhei
  module Stages

    class Enumerator
      def initialize(id:)
        @id = id
      end

      def call(ip)
        enum = ip.context.get_or_set("Rhei.Stages.Enumerator/#{@id}") { ip_data_to_enum(ip) }
        ip.set_data(enum.next)
      rescue StopIteration
        ip.route(:stop_iteration)
      end

      private

      def ip_data_to_enum(ip)
        case ip.data
        when Enumerable
          ip.data.to_enum
        else
          [ip.data]
        end
      end

    end

  end
end

