module Rhei
  module Stages
    class Buffer
      def initialize(id:, route:, max_size: 10240)
        @id, @route, @max_size = id, route, max_size
      end

      def call(ip)
        buffer = ip.context.get_or_set("Rhei.Stages.Buffer/#{@id}") { [] }
        if buffer.size < @max_size
          buffer << ip
          Rhei::IP.new(route: :buffering, context: ip.context)
        else
          ip.context.del("Rhei.Stages.Buffer/#{@id}")
          ip.route(:flush)
          ip.set_data(buffer)
        end
      end

    end
  end
end

