require 'thread'

module Rhei
  module Engine

    class SingleThreadedStack

      def self.root_context
        Thread.current[:"Rhei.Context"] ||= Rhei::Context.new
      end

      def self.finalize(single_threaded_stack)
        return unless root = Thread.current[:"Rhei.Context"]
        root.del("Rhei.Engine.SingleThreadedStack/#{self.object_id}")
      end

      def initialize(group:)
        @group = group
        @stacked_schedule = Rhei::StackedSchedule.new(schedule: group.schedule)
        ObjectSpace.define_finalizer(self, proc { self.class.finalize(self) })
      end

      def run
        engine_context.set(:signal_queue, Queue.new)
        loop do
          break if stopped?
          if next_value = @stacked_schedule.shift_schedule
            label, due_at, taken_at = next_value
            stage = @group.get_stage(label)
            invoke(stage)
          end
          sleep 1 unless stopped?
        end
      end

      def stopped?
        return false unless signal_queue = engine_context.get(:signal_queue)
        signal_queue.closed?
      end

      def stop
        return unless signal_queue = engine_context.get(:signal_queue)
        signal_queue.close
      end

      def invoke(stage, input = Rhei::IP.new)
        ensure_services
        stack = [[stage, input]]
        next_ip = nil
        while (stage, ip = stack.pop) do
          ip.context.set(:services, services_context[".*"])
          next_ip = stage.call(ip)
          @group.next_stages_for(stage.name, route: next_ip.route).each do |next_stage|
            stack.push([next_stage, next_ip])
          end
          break if stopped?
        end
        next_ip
      end

      def engine_context
        id = "Rhei.Engine.SingleThreadedStack/#{self.object_id}"
        self.class.root_context.get_or_set(id) { Rhei::Context.new }
      end

      def ensure_services
        @ensure_services ||= begin
          @group.services.each do |service_ref|
            services_context.get_or_set(service_ref.name) { service_ref.instance }
          end
          true
        end
      end

      def services_context
        self.class.root_context.get_or_set(:services) { Rhei::Context.new }
      end

    end

  end
end

