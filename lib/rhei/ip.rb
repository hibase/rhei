module Rhei

  class IP
    attr_reader :route

    def initialize(attributes: {}, data: nil, route: :ok, context: nil)
      @attributes = attributes
      @data = data
      @route = route
      initialize_context(context)
    end

    def route(new_route = nil)
      return @route unless new_route
      self.next.tamper { |a, d, route, c| [a, d, new_route, c] }
      self
    end

    def [](expr, *modifiers)
      Rhei::KeyExpr.new(expr, modifiers).apply(@attributes)
    end

    def tamper
      return unless block_given?
      @attributes, @data, @route = yield(@attributes, @data, @route)
      self
    end

    def next
      @next ||= IP.new(attributes: @attributes.dup, data: @data, context: @context)
    end

    def set(key, value)
      self.next.tamper do |attributes, data, route|
        attributes[key] = value
        [attributes, data, route]
      end
      self
    end

    def set_data(data)
      self.next.tamper do |attributes, current_data, route|
        [attributes, data, route]
      end
      self
    end

    def data
      @data
    end

    def context
      @context
    end

    def error?
      route == :error
    end

    def ok?
      route == :ok
    end

    def reraise!
      attributes = self["exception.*", :strip]
      return if attributes.empty?
      e = attributes["instance"]
      e.set_backtrace(attributes["backtrace"])
      raise e
    end

    private

    def initialize_context(context)
      return @context = context if context.is_a?(Rhei::Context)
      @context = Rhei::Context.new(context)
    end
  end

end

