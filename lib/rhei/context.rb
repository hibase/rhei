module Rhei
  class Context
    def initialize(store = nil)
      @mutex = Mutex.new
      initialize_store(store)
    end

    def synchronize
      return unless block_given?
      return yield if @mutex.owned?
      @mutex.synchronize { yield }
    end

    def [](expr, *flags)
      synchronize do
        Rhei::KeyExpr.new(expr, flags).apply(@store)
      end
    end

    def with(key)
      synchronize do
        yield(@store[key]) if @store.has_key?(key)
      end
    end

    def get(key)
      synchronize { @store[key.freeze] }
    end

    def set(key, value)
      synchronize { @store[key.freeze] = value }
    end

    def get_or_set(key, value = nil)
      value = block_given? ? yield : value
      synchronize do
        @store[key] ||= value
        @store.delete(key) if @store[key].nil?
        @store[key]
      end
    end

    def del(key)
      synchronize do
        @store.delete(key)
      end
    end

    def ==(other)
      case other
      when Rhei::Context
        self[".*"] == other[".*"]
      when Hash
        self[".*"] == other
      else
        false
      end
    end

    def ===(other)
      other.class == self.class \
        && other.object_id == self.object_id \
        && self.==(other)
    end

    private

    def initialize_store(store)
      case store
      when Rhei::Context
        @store = store[".*"]
      when Hash
        @store = store.dup
      when NilClass
        @store = {}
      else
        raise ArgumentError, "store must be kind of Hash or Rhei::Context"
      end
    end
  end
end

