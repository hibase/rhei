require 'parse-cron'

module Rhei
  class StackedSchedule
    def initialize(schedule: {})
      @stack = []
      initialize_schedule(schedule)
    end

    def add(label, cron, now: Time.now)
      @schedule[label] ||= begin
        due_at = cron.next(now)
        @stack << [label, due_at]
        cron
      end
      self
    end

    def reorder
      @stack.sort_by! { |(label, due_at)| due_at }
      self
    end

    def shift_schedule
      return if @stack.empty?
      return unless next_due?
      taken_at = Time.now
      label, due_at = @stack.shift
      reschedule(label)
      [label, due_at, taken_at]
    end

    def next_due?
      _, due_at = peek
      due_at && Time.now >= due_at
    end

    def peek
      @stack.first
    end

    private

    def reschedule(label)
      cron = @schedule[label]
      label_due_at = cron.next
      index = @stack.each_with_index.inject(-1) do |m, ((_, due_at), i)|
        if due_at <= label_due_at
          i
        elsif due_at > label_due_at
          break m
        end
      end
      @stack.insert(index+1, [label, label_due_at])
    end

    def initialize_schedule(schedule)
      @schedule = {}
      return if schedule.empty?
      now = Time.now
      schedule.each do |name, cron_expression|
        cron = CronParser.new(cron_expression)
        add(name, cron, now: now)
      end
      reorder
    end
  end
end

