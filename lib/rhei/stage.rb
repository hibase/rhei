module Rhei
  class Stage

    attr_reader :kind, :name, :klass, :spec, :lang

    def initialize(kind:, name:, spec: nil, lang: nil)
      (@kind = kind).freeze
      (@name = name).freeze
      @lang = Rhei::Loader.cast_kind(lang) if lang
      @klass = Rhei::Loader.cast_kind(kind)
      @spec = Rhei::Spec.new(spec, klass: klass)
    end

    def build(ip)
      spec = eval_spec(ip)
      spec.instance
    end

    def call(ip)
      instance = build(ip)
      instance.call(ip)
      ip.next
    rescue Exception => e
      ip.route(:error)
      ip.set("exception.stage", name)
      ip.set("exception.instance", e)
      ip.set("exception.class_name", e.class.name)
      ip.set("exception.message", e.message)
      ip.set("exception.backtrace", e.backtrace)
      ip.next
    end

    private

    def eval_spec(ip)
      return @spec unless @lang
      @spec.validate!
      data = ip.data
      attributes = ip[".*"]
      services = ip.context["services.*"] || {}
      services = services["services"]
      new_spec = @spec.spec.inject({}) do |m, (key, value)|
        m[key] = @lang.eval(value, data: data, attributes: attributes, services: services)
        m
      end
      Rhei::Spec.new(new_spec, klass: @klass)
    end

  end
end

