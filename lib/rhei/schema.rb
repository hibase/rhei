require_relative '../simple_singleton.rb'
require 'json_schemer'

module Rhei
  class Schema
    class Invalid < StandardError; end

    include SimpleSingleton

    DEFINITIONS = {
      "kind" => {
        "type" => "string",
        "pattern" => '^[a-zA-Z]+[a-zA-Z0-9]*(\.[a-zA-Z]+[a-zA-Z0-9]*)*$'
      },

      "name" => {
        "type" => "string",
        "pattern" => '^[a-z0-9_]*$'
      },

      "attributeName" => {
        "pattern" => '^[a-z_]+(\.[a-zA-Z0-9_-]+)*$'
      },

      "attributes" => {
        "type" => "object",
        "propertyNames" => {
          "pattern" => '^[a-z_]+(\.[a-zA-Z0-9_-]+)*$'
        }
      },

      "spec" => {
        "type" => "object",
        "propertyNames" => {
          "pattern" => '^[a-z_]+$'
        }
      },

      "Rhei.Object" => {
        "type" => "object",
        "properties" => {
          "kind" => { "$ref" => "#/definitions/kind" },
          "name" => { "$ref" => "#/definitions/name" },
          "spec" => { "type" => "object" }
        },
        "required" => ["kind", "name", "spec"]
      }
    }

    def rhei_object_schema(schema: nil, spec: nil)
      schema = (schema.kind_of?(Array) ? schema : [schema]).compact
      spec = (spec.kind_of?(Array) ? spec : [spec]).compact
      {
        "definitions" => DEFINITIONS,
        "allOf" => [
          { "$ref" => "#/definitions/Rhei.Object" },
          *schema,
          {
            "type" => "object",
            "properties" => {
              "spec" => {
                "allOf" => [
                  { "$ref" => "#/definitions/spec" },
                  *spec
                ]
              }
            }
          }
        ]
      }
    end

    def validate(object, schema: nil, spec: nil)
      schemer = JSONSchemer.schema(
        rhei_object_schema(schema: schema, spec: spec)
      )
      schemer.validate(object).to_a
    end

    def valid?(object)
      validate(object).empty?
    end
  end
end

