module Rhei
  class Spec
    class Invalid < StandardError; end

    attr_reader :spec, :klass, :parameters

    def initialize(spec = nil, klass:)
      @spec = spec || {}
      @klass = klass
      @errors = Hash.new { |h, k| h[k] = [] }
      initialize_parameters
    end

    def instance
      validate!
      klass.new(*parameters)
    end

    def errors
      @errors.dup
    end

    def valid?
      @errors.empty?
    end

    def validate!
      return if valid?
      raise Invalid, error_message
    end

    def error_message
      "got #{@errors.size} errors. %s" % (
        @errors.map { |kind, names| "#{kind}: #{names.size} (#{names.join(", ")})" }.join("; ")
      )
    end

    private

    def error(kind, name_or_message)
      @errors[kind] |= [name_or_message.to_s]
    end

    def initialize_parameters
      unless @spec.kind_of?(Hash)
        error(:spec_type, "spec must be kind of Hash, got: #{@spec.class.name}")
        return
      end
      params = @klass.instance_method(:initialize).parameters
      @parameters = params.reduce([]) do |list, (type, key)|
        name = key.to_s
        value = @spec[name]
        case type
        when :keyreq
          error(:missing, name) if value.nil?
          list.last.is_a?(Hash) ?
            list.last[key] = value :
            list << {key => value}
          list
        when :opt
          next list if value.nil?
          list << @spec[name]
        when :key
          next list if value.nil?
          list.last.is_a?(Hash) ?
            list.last[key] = value :
            list << {key => value}
          list
        when :req
          error(:missing, name) if value.nil?
          list << value
        when :rest
          next list if value.nil?
          value.is_a?(Array) ?
            list.concat(value) :
            raise(Invalid, "spec `#{name}' must be kind of Array, got: #{value.class.name}")
        end
      end.freeze
    end

  end
end

