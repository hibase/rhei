require 'singleton'

module SimpleSingleton
  module ClassMethods
    def respond_to_missing?(method, include_private = false)
      super || instance.respond_to?(method)
    end

    def method_missing(method, *args, &block)
      instance.send(method, *args, &block)
    end
  end

  def self.included(base)
    base.send(:include, Singleton)
    base.extend(ClassMethods)
  end
end

