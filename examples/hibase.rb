require 'typhoeus'
require 'oj'
require 'jq'

require 'pry-byebug'

module Hibase
  class HTTPRequest

    def initialize(attributes)
      @attributes = attributes
      @request = Typhoeus::Request.new(
        attributes["url"],
        method: attributes["method"]&.to_sym,
        body: attributes["body"],
        params: Rhei::KeyExpr.new("get_params.*", :strip).apply(attributes),
        headers: Rhei::KeyExpr.new("headers.*", :strip).apply(attributes)
      )
    end

    def call(ip)
      case response = @request.run
      when (lambda &:success?)
        ip.route(:ok)
      else
        ip.route(:error)
      end
      ip.set("http_request.response.code", response.code)
      response.headers.each { |key, value| ip.set("http_request.response.headers.#{key.downcase}", value) }
      ip.set_data(response.body)
      ip
    end
  end

  class JSONParse
    def call(ip)
      data = Oj.load(ip.data)
      ip.set_data(data)
    end
  end

  class Inspect
    def call(ip)
      puts <<~TEXT
        route:
          #{ip.route}
        context:
          #{ip.instance_variable_get(:@context).inspect}
        attributes:
          #{Oj.dump(ip.instance_variable_get(:@attributes)).gsub("\n", "\n  ")}
        data:
          #{ip.data.inspect}
      TEXT
      ip
    end
  end

  class Record
    class Invalid < StandardError; end

    attr_reader :columns, :values

    def initialize(columns:, values:)
      raise Invalid, "number of columns do not match values" \
        unless columns.size == values.size
      @columns, @values = columns, values
    end
  end


  class HashToRecord
    def initialize(columns:, values:)
      @columns, @values = columns, values
    end

    def call(ip)
      record = Record.new(
        columns: @columns,
        values: @values
      )
      ip.set_data(record)
    end
  end

end

