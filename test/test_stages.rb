class TestStage
  def call(ip)
    ip.set_data("test")
  end
end

class ArrayFiveStage
  def call(ip)
    ip.set_data([1, 2, 3, 4, 5])
  end
end

class ErrorStage
  def call(ip)
    raise Exception, "boom"
  end
end

class ThreadTestStage
  @mutex = Mutex.new
  @count = 0

  def self.count
    @mutex.synchronize { @count }
  end

  def self.add
    @mutex.synchronize { @count += 1 }
  end

  def call(ip)
    self.class.add
    ip
  end
end

