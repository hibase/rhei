require 'bundler'
Bundler.setup(:default, :test)
require 'minitest/spec'
require 'minitest/autorun'
require 'minitest/pride'
require 'pry-byebug'
require 'timecop'

require_relative '../lib/rhei.rb'

def silence_stderr
  stderr = $stderr
  $stderr = StringIO.new
  yield
ensure
  $stderr = stderr
end

