require 'test_helper'

describe Rhei::IP do
  subject { Rhei::IP.new(data: "some data", attributes: { "foo" => "bar", "zig.zag" => "clap", "zig.zug" => "blub" }) }

  describe '#[]' do
    it 'returns a single value from the attributes' do
      assert_equal "bar", subject["foo"]
    end

    it 'returns nil for non-existent attributes' do
      assert_nil subject["nothing"]
    end

    it 'returns a selection of attributes for dot-star keys' do
      assert_equal Hash[
        "zig.zag" => "clap",
        "zig.zug" => "blub"
      ], subject["zig.*"]
    end

    it 'returns an empty hash for non-existent dot-star keys' do
      result = subject["nothing.*"]
      assert_equal Hash.new, result
    end

    it 'returns the whole set of attributes for dot-star' do
      assert_equal Hash[
        "foo" => "bar",
        "zig.zag" => "clap",
        "zig.zug" => "blub"
      ], subject[".*"]
    end

    it 'returns dupped attributes that do not modify the attributes in the IP' do
      value = subject["foo"]
      value.upcase!

      assert_equal "bar", subject["foo"]
      assert_equal "BAR", value
    end

    it 'returns a dupped selection that does not allow modification in the IP' do
      hash = subject["zig.*"]
      hash.each { |k, v| v.upcase! }

      assert_equal ["CLAP", "BLUB"], hash.values
      assert_equal ["clap", "blub"], subject["zig.*"].values
    end

    describe 'modifier :strip' do
      it 'returns a selection of attributes with their keys stripped for dot-star keys' do
        assert_equal Hash[
          "zag" => "clap",
          "zug" => "blub"
        ], subject["zig.*", :strip]
      end
    end
  end

  describe "#next" do
    it 'returns a successor to the current IP, both in spirit and soul' do
      refute_equal subject.object_id, subject.next.object_id

      assert_equal subject[".*"], subject.next[".*"]
      assert_equal subject.data, subject.next.data
      assert_equal subject.context.object_id, subject.next.context.object_id
      assert_equal subject.context, subject.next.context
    end
  end

  describe "#set" do
    it 'sets a data on the given key for the sucessors attributes' do
      subject.set("foo", "something else than bar")

      assert_equal "something else than bar", subject.next["foo"]
    end

    it 'does not modify the existing attributes' do
      subject.set("foo", "something else than bar")

      assert_equal "bar", subject["foo"]
    end
  end

  describe "#set_value" do
    it 'sets a value for the successor' do
      subject.set_data("new value")
      assert_equal "some data", subject.data

      assert_equal "new value", subject.next.data
    end
  end
end

