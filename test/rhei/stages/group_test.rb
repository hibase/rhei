require 'test_helper'
require 'test_stages'

describe Rhei::Stages::Group do
  let(:stages) { nil }
  subject { Rhei::Stages::Group.new(stages: stages) }

  describe '#add_stage' do
    it 'adds the given stage to the group by name' do
      subject.add_stage("name" => "hey", "kind" => "TestStage")
      assert_kind_of Rhei::Stage, subject.get_stage("hey")
    end

    it 'raises an error if the stage already exists' do
      subject.add_stage("name" => "boom", "kind" => "TestStage")
      assert_raises(Rhei::Stages::Group::Invalid, "stage with name `boom' already exists") do
        subject.add_stage("name" => "boom", "kind" => "Rhei.Stages.Passthrough")
      end
    end
  end

  describe '#connect' do
    let(:stages) { [{"name" => "IN", "kind" => "Rhei.Stages.Passthrough"}, {"name" => "OUT", "kind" => "Rhei.Stages.Passthrough"}] }

    it 'adds a connection for the given stage name to another stage' do
      subject.connect("OUT", to: "IN")
      out_stage, _ = subject.next_stages_for("IN")
      assert_equal "OUT", out_stage.name
    end

    it 'adds a connection for the given stage name on a specific route' do
      subject.connect("OUT", to: "IN", route: :error)
      stages = subject.next_stages_for("IN")
      assert_empty stages

      out_stage, _ = subject.next_stages_for("IN", route: :error)
      assert_equal "OUT", out_stage.name
    end

    it 'raises an error if the given stage name does not exist' do
      assert_raises(Rhei::Stages::Group::StageNotFound, "stage with name `boom' does not exist") do
        subject.connect("boom", to: "OUT")
      end

      assert_raises(Rhei::Stages::Group::StageNotFound, "stage with name `boom' does not exist") do
        subject.connect("OUT", to: "boom")
      end
    end
  end

  describe '#enumerate' do
    let(:stages) { [{"name" => "IN", "kind" => "ArrayFiveStage"}, {"name" => "OUT", "kind" => "Rhei.Stages.Passthrough"}] }

    #  +-----+
    #  |     |
    #  | IN  |
    #  |     |
    #  +--|--+
    #     |
    #     |                +--------------------------+
    #     |                |                          |
    #     +----route: ok---> Rhei::Stages::Enumerator ----+
    #                      |                          |   |
    #                      +-|---------|--^-----------+   |
    #                        |         |  |               |
    #  +-----+               |         |  |               |
    #  |     |               |         |  +---route: ok---+
    #  | OUT <----route: R---+         |
    #  |     |                         |
    #  +--|--+                         route: stop_iteration
    #     |                            |
    #     |                            |
    #     v                            v

    it 'adds a connection for the given stage name to an Rhei::Stages::Enumerator stage' do
      subject.enumerate("OUT", from: "IN")

      enumerator_stage, _ = subject.next_stages_for("IN")
      assert_equal Rhei::Stages::Enumerator, enumerator_stage.klass

      enumerator_stage, out_stage = subject.next_stages_for(enumerator_stage.name, route: :ok)
      assert_equal Rhei::Stages::Enumerator, enumerator_stage.klass
      assert_equal Rhei::Stages::Passthrough, out_stage.klass
    end
  end

  describe '#buffer' do
    let(:stages) { [{"name" => "IN", "kind" => "ArrayFiveStage"}, {"name" => "pass", "kind" => "Rhei.Stages.Passthrough"}] }

    #  +-----+
    #  |     |
    #  |  IN +---route: R----+
    #  |     |               |
    #  +-----+               |
    #                        |
    #                      +-v--------------------+
    #                      |                      |
    #                      | Rhei::Stages::Buffer |
    #                      |                      |
    #                      +-----|-----|----------+
    #                            |     |
    #  +-----+                   |     |
    #  |     |                   |     |
    #  | OUT <----route: flush---+     |
    #  |     |                         |
    #  +--|--+                         route: buffering
    #     |                            |
    #     |                            |
    #     v                            v

    it 'adds a connection for the given stage to an Rhei::Stages::Buffer stage' do
      subject.enumerate("pass", from: "IN")
      subject.buffer("OUT", from: "pass", spec: {"max_size" => 3})

      buffer_stage, _ = subject.next_stages_for("pass", route: :ok)
      assert_equal Rhei::Stages::Buffer, buffer_stage.klass

      out_stage, _ = subject.next_stages_for(buffer_stage.name, route: :flush)
      assert_equal Rhei::Stages::Passthrough, out_stage.klass
    end
  end

  describe '#get_stage' do
    let(:stages) { [{"name" => "IN", "kind" => "Rhei.Stages.Passthrough"}] }
    subject { Rhei::Stages::Group.new(stages: stages) }

    it 'returns the stage of the given name' do
      assert_kind_of Rhei::Stage, subject.get_stage("IN")
    end

    it 'raises an error if the given stage does not exist' do
      assert_raises(Rhei::Stages::Group::StageNotFound, "stage with name `boom' does not exist") do
        subject.get_stage("boom")
      end
    end
  end

  describe '#call' do
    let(:stages) { [{"name" => "IN", "kind" => "TestStage"}] }
    subject { Rhei::Stages::Group.new(stages: stages) }

    it 'invokes the IN-stage with the configured engine' do
      result = subject.call
      result.reraise!
      assert_kind_of Rhei::IP, result
      assert_equal "test", result.data
    end

    it 'passes the given IP onto the IN-stage with the configured engine' do
      result = subject.call(Rhei::IP.new(context: {this: "that"}))
      result.reraise!
      assert_kind_of Rhei::IP, result
      assert_equal "that", result.context.get(:this)
    end
  end

end

