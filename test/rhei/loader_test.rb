require 'test_helper'

describe Rhei::Loader do
  let(:root) { File.expand_path("../loader_test", __FILE__) }

  class IAmLoaded; end

  describe '::require_file' do
    let(:path) { "#{root}/single_load.rb" }

    it 'loads a file at most once' do
      Rhei::Loader.require_file(path)
      assert_includes Rhei::Loader.required_paths, path

      silence_stderr { Rhei::Loader.require_file(path) }
      assert_equal 1, RHEI_LOADER_TEST[path]
    end
  end

  describe ':.load_file' do
    let(:path) { "#{root}/multiple_loads.rb" }

    it 'loads a given ruby file' do
      Rhei::Loader.load_file(path)
      assert_equal 1, RHEI_LOADER_TEST[path]

      Rhei::Loader.load_file(path)
      assert_equal 2, RHEI_LOADER_TEST[path]

      Rhei::Loader.load_file(path)
      assert_equal 3, RHEI_LOADER_TEST[path]
    end

    it 'loads a given yml declaration' do
      declaration = Rhei::Loader.load_file("#{root}/declaration.yml")
      assert_kind_of Rhei::Declaration, declaration

      declaration = Rhei::Loader.load_file("#{root}/declaration.yaml")
      assert_kind_of Rhei::Declaration, declaration
    end

    it 'loads a given json declaration' do
      declaration = Rhei::Loader.load_file("#{root}/declaration.json")
      assert_kind_of Rhei::Declaration, declaration
    end

    it 'raises an error when trying to load other types' do
      assert_raises(Rhei::Loader::Invalid) { Rhei::Loader.load_file("#{root}/catz.gif") }
    end
  end

  describe '::lookup_kind' do
    let(:kind) { IAmLoaded.name.gsub('::', '.') }

    it 'looks up a klass by its kind-path translated to ruby class paths' do
      assert_equal IAmLoaded, Rhei::Loader.lookup_kind(kind)
    end

    it 'looks up declared kinds from loaded declarations' do
      Rhei::Loader.load_file("#{root}/declaration.yml")
      assert_equal Rhei::Declaration, Rhei::Loader.lookup_kind('Rhei.LoaderTest.Declaration')
    end
  end
end

