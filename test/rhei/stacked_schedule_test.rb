require 'test_helper'

describe Rhei::StackedSchedule do
  let(:schedule) { Hash[foo: "* * * * *", bar: "0 #{(Time.now.hour + 1) % 24} * * *"] }
  let(:stacked_schedule) { Rhei::SingleThreadedStack::StackedSchedule.new(schedule: schedule) }
  before { stacked_schedule }

  describe '#next_due?' do
    it 'returns false on an empty schedule' do
      stacked_schedule = Rhei::StackedSchedule.new
      refute stacked_schedule.next_due?
    end

    it 'tells if the next item on the schedule is due' do
      cron = CronParser.new(schedule[:foo])
      Timecop.freeze(cron.next - 1) do
        refute stacked_schedule.next_due?
      end
      Timecop.freeze(cron.next) do
        assert stacked_schedule.next_due?
      end
    end
  end

  describe '#blocking_shift_schedule' do
    it 'fetches the first item from the stack and pushes the next run at the end' do
      cron = CronParser.new(schedule[:foo])
      expected_due_at = cron.next

      Timecop.travel(expected_due_at - 0.2) do
        label, due_at, taken_at = stacked_schedule.blocking_shift_schedule
        assert_equal :foo, label
        assert_equal expected_due_at, due_at
        assert_in_delta expected_due_at, taken_at, 0.99

        label, due_at = stacked_schedule.peek
        assert_equal :foo, label
        assert_equal cron.next, due_at

        cron = CronParser.new(schedule[:bar])
        expected_due_at = cron.next

        Timecop.travel(expected_due_at - 60) { stacked_schedule.blocking_shift_schedule }
        Timecop.travel(expected_due_at) do
          label, due_at, taken_at = stacked_schedule.blocking_shift_schedule
          assert_equal :bar, label
          assert_equal expected_due_at, due_at
        end
      end
    end

    it 'returns nil and breaks the waiting loop if the given block returns true' do
      assert_nil stacked_schedule.blocking_shift_schedule { true }
    end

    it 'returns nil if the stack is empty' do
      assert_nil Rhei::StackedSchedule.new.blocking_shift_schedule
    end
  end

end

