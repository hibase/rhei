require 'test_helper'

describe Rhei::Spec do
  let(:spec)              { Hash["foo" => "bar", "zig" => "zag"] }
  let(:spec_partial)      { spec.merge("blu" => "ber") }
  let(:spec_full)         { spec.merge("blu" => "ber", "muh" => "meh") }
  let(:spec_false)        { spec.merge("blu" => false) }
  let(:spec_args)         { spec.merge("args" => [1, 2, 3]) }
  let(:spec_partial_args) { spec.merge("args" => [1, 2, 3], "blu" => "ber") }

  def parameters(spec, klass)
    spec = Rhei::Spec.new(spec, klass: klass)
    spec.validate!
    spec.parameters
  end

  class None; end
  class List;                            def initialize(foo, zig); end; end
  class Keyword;                         def initialize(foo:, zig:); end; end
  class ListKeyword;                     def initialize(foo, zig:); end; end

  class ListOptional;                    def initialize(foo, zig, blu = nil); end; end
  class LeftOptionalList;                def initialize(blu = nil, foo, zig); end; end
  class KeywordOptional;                 def initialize(foo:, zig:, blu: nil); end; end
  class LeftOptionalKeyword;             def initialize(blu = nil, foo:); end; end
  class LeftOptionalListKeyword;         def initialize(blu = nil, foo, zig:); end; end
  class ListKeywordOptional;             def initialize(foo, zig, blu: nil); end; end
  class LeftOptionalKeywordOptional;     def initialize(muh = nil, blu: nil); end; end
  class LeftOptionalListKeywordOptional; def initialize(muh = nil, foo, zig:, blu: nil); end; end

  class Splat;                           def initialize(*args); end; end
  class ListSplat;                       def initialize(foo, *args); end; end
  class SplatKeyword;                    def initialize(*args, foo:); end; end
  class SplatKeywordOptional;            def initialize(*args, blu: nil); end; end
  class LeftOptionalSplatKeyword;        def initialize(blu = nil, *args, foo:); end; end

  it { assert_raises(Rhei::Spec::Invalid, "got 1 errors. spec_type: spec must be kind of Hash, got: Array") { parameters([], None) } }

  it { assert_equal [], parameters(nil, None) }

  it { assert_equal [],                         parameters(spec, None) }
  it { assert_equal ["bar", "zag"],             parameters(spec, List) }
  it { assert_equal [{foo: "bar", zig: "zag"}], parameters(spec, Keyword) }
  it { assert_equal ["bar", {zig: "zag"}],      parameters(spec, ListKeyword) }

  describe 'missing' do
    let(:spec_missing) { Hash["foo" => "bar"] }

    it { assert_equal [], parameters(spec_missing, None) }

    it { assert_raises(Rhei::Spec::Invalid, "spec `zig' must be present, got: nil") { parameters(spec_missing, List) } }
    it { assert_raises(Rhei::Spec::Invalid, "spec `zig' must be present, got: nil") { parameters(spec_missing, Keyword) } }
  end

  describe 'optional' do
    it { assert_equal ["bar", "zag"],             parameters(spec, ListOptional) }
    it { assert_equal ["bar", "zag"],             parameters(spec, LeftOptionalList) }
    it { assert_equal [{foo: "bar", zig: "zag"}], parameters(spec, KeywordOptional) }
    it { assert_equal [{foo: "bar"}],             parameters(spec, LeftOptionalKeyword) }
    it { assert_equal ["bar", {zig: "zag"}],      parameters(spec, LeftOptionalListKeyword) }
    it { assert_equal ["bar", "zag"],             parameters(spec, ListKeywordOptional) }
    it { assert_equal [],                         parameters(spec, LeftOptionalKeywordOptional) }
    it { assert_equal ["bar", {zig: "zag"}],      parameters(spec, LeftOptionalListKeywordOptional) }

    it { assert_equal ["bar", "zag", "ber"],                  parameters(spec_partial, ListOptional) }
    it { assert_equal ["ber", "bar", "zag"],                  parameters(spec_partial, LeftOptionalList) }
    it { assert_equal [{foo: "bar", zig: "zag", blu: "ber"}], parameters(spec_partial, KeywordOptional) }
    it { assert_equal ["ber", {foo: "bar"}],                  parameters(spec_partial, LeftOptionalKeyword) }
    it { assert_equal ["ber", "bar", {zig: "zag"}],           parameters(spec_partial, LeftOptionalListKeyword) }
    it { assert_equal ["bar", "zag", {blu: "ber"}],           parameters(spec_partial, ListKeywordOptional) }
    it { assert_equal [{blu: "ber"}],                         parameters(spec_partial, LeftOptionalKeywordOptional) }
    it { assert_equal ["bar", {zig: "zag", blu: "ber"}],      parameters(spec_partial, LeftOptionalListKeywordOptional) }

    it { assert_equal ["meh", {blu: "ber"}],                    parameters(spec_full, LeftOptionalKeywordOptional) }
    it { assert_equal ["meh", "bar", {zig: "zag", blu: "ber"}], parameters(spec_full, LeftOptionalListKeywordOptional) }

    it { assert_equal ["bar", "zag", false],                  parameters(spec_false, ListOptional) }
    it { assert_equal [{foo: "bar", zig: "zag", blu: false}], parameters(spec_false, KeywordOptional) }
    it { assert_equal [false, "bar", "zag"],                  parameters(spec_false, LeftOptionalList) }
  end

  describe 'splat' do
    it { assert_equal [],             parameters(spec, Splat) }
    it { assert_equal ["bar"],        parameters(spec, ListSplat) }
    it { assert_equal [{foo: "bar"}], parameters(spec, SplatKeyword) }
    it { assert_equal [],             parameters(spec, SplatKeywordOptional) }
    it { assert_equal [{foo: "bar"}], parameters(spec, LeftOptionalSplatKeyword) }

    it { assert_equal [1, 2, 3],        parameters(spec_args, Splat) }
    it { assert_equal ["bar", 1, 2, 3], parameters(spec_args, ListSplat) }

    it { assert_equal [{blu: "ber"}],        parameters(spec_partial, SplatKeywordOptional) }
    it { assert_equal ["ber", {foo: "bar"}], parameters(spec_partial, LeftOptionalSplatKeyword) }

    it { assert_equal [1, 2, 3, {blu: "ber"}],        parameters(spec_partial_args, SplatKeywordOptional) }
    it { assert_equal ["ber", 1, 2, 3, {foo: "bar"}], parameters(spec_partial_args, LeftOptionalSplatKeyword) }

    it 'raises if the value of the splat param is not an array' do
      assert_raises(Rhei::Spec::Invalid, "errors: 1, wrong_type: 1 (args)") do
        parameters({"args" => { "bada" => "boom" }}, Splat)
      end
    end
  end

end

