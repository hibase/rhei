require 'test_helper'

describe Rhei::Schema do
  let(:valid_object) { Hash["kind" => "Some.Object", "name" => "instance", "spec" => {}] }

  describe '#validate' do
    it 'returns an empty list for a valid object' do
      errors = Rhei::Schema.validate(valid_object)
      assert_empty errors
    end

    it 'returns all errors for a given object' do
      errors = Rhei::Schema.validate({
        "kind" => 123,
        "name" => "instance",
        "spec" => nil
      })
      assert_equal 3, errors.size
    end

    describe 'schema:' do
      it 'validates the object with an additional schematic' do
        refute_empty Rhei::Schema.validate(valid_object, schema: {
          "type" => "object",
          "properties" => { "foo" => { "type" => "string" } },
          "required" => ["foo"]
        })
      end
    end

    describe 'spec:' do
      it 'validates the spec section of the object with an additional schematic' do
        refute_empty Rhei::Schema.validate(valid_object, spec: {
          "type" => "object",
          "properties" => { "foo" => { "type" => "string" } },
          "required" => ["foo"]
        })
      end
    end
  end

  describe '#valid?' do
    it 'returns true if the object is valid' do
      assert Rhei::Schema.valid?({
        "kind" => "Some.Object",
        "name" => "instance",
        "spec" => {}
      })
    end

    it 'returns false if the object is invalid' do
      refute Rhei::Schema.valid?({"kind" => nil})
    end
  end

end

