require 'test_helper'
require 'test_stages'

describe Rhei::Engine::SingleThreadedStack do
  let(:stages) { [
    {"kind" => "TestStage", "name" => "test"},
    {"kind" => "PassthroughStage", "name" => "pass"},
    {"kind" => "ErrorStage", "name" => "boom"}
  ] }
  let(:group) { Rhei::Stages::Group.new(stages: stages, graph: [{"stage" => "pass", "to" => "test"}]) }
  subject { Rhei::Engine::SingleThreadedStack.new(group: group) }

  describe '#invoke' do
    it 'processes an IP starting from the given stage' do
      result = subject.invoke(group.get_stage("pass"), Rhei::IP.new(data: "foo"))
      result.reraise! if result.error?
      assert_kind_of Rhei::IP, result
      assert_equal "foo", result.data
    end

    it 'processes the complete graph' do
      result = subject.invoke(group.get_stage("test"))
      result.reraise! if result.error?
      assert_kind_of Rhei::IP, result
      assert_equal "test", result.data
    end

    it 'captures exceptions and routes them to :error' do
      result = subject.invoke(group.get_stage("boom"))
      assert result.error?
      assert_raises(Exception, "boom") { result.reraise! }
      assert_kind_of Rhei::IP, result
      assert_equal "Exception", result["exception.class_name"]
      assert_equal "boom", result["exception.message"]
      assert_kind_of Exception, result["exception.instance"]
    end
  end

  describe '#run' do
    let(:schedule) { "* * * * *" }
    let(:stages) { [{"kind" => "ThreadTestStage", "name" => "test"}] }
    let(:group) { Rhei::Stages::Group.new(stages: stages, schedule: { "test" => schedule }) }

    it 'starts a scheduler for all groups with a schedule' do
      now = Time.now
      cron = CronParser.new(schedule)
      Timecop.freeze(cron.last(now)) do
        subject # initialize everything

        Timecop.freeze(cron.next(now)) do
          thread = Thread.new {
            Thread.abort_on_exception = true
            subject.run
          }
          thread.join(0.5)
          subject.stop
          assert_equal 1, ThreadTestStage.count
        end
      end
    end

  end

end

