require 'test_helper'

describe Rhei::Declaration do

  let(:kind) { "#{Something.name.gsub("::", ".")}" }
  let(:spec) { Hash["foo" => "bar"] }
  subject { Rhei::Declaration.new(kind: kind, spec: spec, path: __FILE__) }

  class Something
    attr_reader :foo, :opt

    def initialize(foo, opt: nil)
      @foo = foo
      @opt = opt || "default"
    end
  end

  describe '::new' do
    it 'initializes a new declaration' do
      assert_kind_of Rhei::Declaration, subject
    end

    it 'loads additional declarations relative to the given path' do
      # this test has a folder next to it with a small example declaration something.yml
      include_path = "declaration_test/something.yml"
      expanded_path = File.expand_path(File.join(File.dirname(__FILE__), include_path))

      declaration = Rhei::Declaration.new(kind: kind, spec: spec, includes: include_path, path: __FILE__)

      assert_equal [expanded_path], declaration.includes
    end

    it 'declares itself to Rhei::Loader' do
      assert_equal subject, Rhei::Loader.get_declaration(kind)
    end

    describe 'declares:' do
      subject { Rhei::Declaration.new(kind: kind, declares: "SomethingDifferent", spec: spec, path: __FILE__) }

      it 'declares itself as a different kind to Rhei::Loader' do
        assert_equal subject, Rhei::Loader.get_declaration("SomethingDifferent")
      end
    end
  end

  describe '#instance' do
    it 'returns an instance of the declared kind' do
      instance = subject.instance
      assert_kind_of Something, instance
    end

    it 'initializes the instances with the given spec' do
      instance = subject.instance
      assert_equal "bar", instance.foo
      assert_equal "default", instance.opt
    end

    describe 'invalid spec' do
      let(:spec) { Hash["opt" => "but no foo"] }

      it 'raises Rhei::Spec::Invalid' do
        assert_raises(Rhei::Spec::Invalid, "got 1 errors. missing: 1 (foo)") { subject.instance }
      end
    end
  end

end

